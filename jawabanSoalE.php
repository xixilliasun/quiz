<?php
echo "
SELECT 
customers.name AS customer_name, SUM(amount) AS total_amount 
FROM customers 
JOIN orders ON customers.id = orders.customer_id 
GROUP BY customers.name 
ORDER BY customers.id ASC;
";
?>