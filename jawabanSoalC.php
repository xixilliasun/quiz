<?php

echo "
CREATE TABLE customers ( 
id INT NOT NULL AUTO_INCREMENT , 
name VARCHAR(255) NOT NULL , 
email VARCHAR(255) NOT NULL , 
password VARCHAR(255) NOT NULL , PRIMARY KEY (id));

CREATE TABLE orders ( 
id INT NOT NULL AUTO_INCREMENT , 
amount INT NOT NULL , 
customer_id INT NOT NULL , 
PRIMARY KEY (id),
FOREIGN KEY (customer_id) REFERENCES customers(id)
);
";

?>

