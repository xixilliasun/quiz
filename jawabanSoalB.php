<?php
function perolehan_medali($arr)
{
	if(empty($arr)) 
	    echo "No data<br>"; 
	  
	else{
	   	$arrayBaru = array();
	   	
	   	for ($i=0; $i < count($arr) ; $i++) {
	   		$arrayBaru[] = array(
	   			'negara' => $arr[$i][0],
	   			'emas' => 4,
	   			'perak' => 1,
	   			'perunggu'=>0
	   		);
	   	} 
	   	return $arrayBaru;
	}
}
echo "<pre>";
print_r(perolehan_medali(
	array(
		array('Indonesia','emas'),
		array('India','perak'),
		array('Korea Selatan','emas'),
		array('India','perak'),
		array('India','emas'),
		array('Indonesia','perak'),
		array('Indonesia','emas'),
	)
));
print_r(perolehan_medali([]));
echo "</pre>";

?>

